## Pilha de tecnologias
```
	* Spring Boot
	* Java 8
	* Maven
	* Sem Banco de dados
	* Este é o server back-end.
	* Já o front-end em Angular 8 é baixado em: https://bitbucket.org/maurecigit/angularfront/get/master.zip
	* O front-end também pode ser baixado com git clone: https://maurecigit@bitbucket.org/maurecigit/angularfront.git
```

## Baixando o projeto do github

```
	Página do bitbucket: https://bitbucket.org/maurecigit/angular/src/master/
	$ mkdir -p /workspace/app-server
	$ cd /workspace/app-server
	$ git clone https://maurecigit@bitbucket.org/maurecigit/angular.git

	Ou dowload: https://bitbucket.org/maurecigit/angular/get/master.zip
```

## Executando a aplicação no eclipse

```
1. Para rodar a aplicação no eclipse [botão direito: Run As/Java Application] em cima do arquivo
"/app-server/src/main/java/br/com/app/Application.java".
```



## Executando no modo standalone

	$ mvn spring-boot:run
	
## Empacotando para deploy

	$ mvn clean package
	# Utilizar o arquivo target/app-server*.war 



## API's disponíveis



Operações para cliente

# 1. * Gerar Senha Normal;
```
HTTP : POST	-	Path : http://localhost:8090/app-server/senha/N

	# Retorno:
	{
	  "tipo": "NORMAL",
	  "sequencial": 1,
	  "prioridade": 10001,
	  "codigo": "N0001"
	}	
```

# 2. * Gerar Senha Preferencial

```
HTTP : POST	-	Path : http://localhost:8090/app-server/senha/P

	# Retorno:
	{
	  "tipo": "PREFERENCIAL",
	  "sequencial": 1,
	  "prioridade": 1,
	  "codigo": "P0001"
	};
```
# 3. * Conferir Última Senha Chamada

```
HTTP : GET	-	Path : http://localhost:8090/app-server/senha

	# Retorno:
	{
	  "tipo": "NORMAL",
	  "sequencial": 1,
	  "prioridade": 10001,
	  "codigo": "N0001"
	};
```
	

 Operações para gerente

# 4. * Reiniciar Senha Normal

```
HTTP : PUT	-	Path : http://localhost:8090/app-server/senha/N

	# Retorno: status HTTP/1.1 200 OK
	
```

# 5. * Reiniciar Senha Preferencial

```
HTTP : PUT	-	Path : http://localhost:8090/app-server/senha/P

	# Retorno: status HTTP/1.1 200 OK
```

# 6. * Chamar Próxima Senha

```
HTTP : POST	-	Path : http://localhost:8090/app-server/senha

	# Retorno: 
	{
	  "tipo": "PREFERENCIAL",
	  "sequencial": 1,
	  "prioridade": 1,
	  "codigo": "P0001"
	}
```
 Os Testes são sempre executados ao compilar a aplicação através do Maven.