package br.com.app.domain;

import br.com.app.errors.CodeInvalid;

public class Password {

	private Integer sequence;
	private Tipo typ;

	public Password(String code) {
		
		try {
			if (code.length() < 5) {
				
				throw new Exception();
			}

			this.sequence = Integer.parseInt(code.substring(1));
			this.typ = Tipo.getBySigla(code.substring(0, 1));
			
		} catch (Exception e) {
			
			throw new CodeInvalid();
		}
	}
	
	public Password(Tipo tp, Integer sequencial) {
		
		super();
		this.sequence = sequencial;
		this.typ = tp;
	}

	public Password() { }


	public Integer getSequencial() {
		
		return sequence;
	}

	public void setSequencial(Integer sequencial) {
		
		this.sequence = sequencial;
	}

	public String getCodigo() {
		
		return typ.getSigla() + String.format("%04d", sequence);
	}

	public Tipo getTipo() {
		
		return typ;
	}
	
	public void setTipo(Tipo tipo) {
		
		this.typ = tipo;
	}
	
	public int getPrioridade() {
		
		return typ.getPrioridade() + sequence;
	}

}
