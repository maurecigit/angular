package br.com.app.domain;

import br.com.app.errors.Invalid;

public enum Tipo {
	
	NORMAL("N", 10000), PREFERENCIAL("P", 0);

	private final int prioridade;
	private final String sigla;

	private Tipo(final String sigla, final int prioridade) {
		
		this.prioridade = prioridade;
		this.sigla = sigla;
	}
	
	public static Tipo getBySigla(final String sigla) {
		
		if (NORMAL.sigla.equals(sigla)) {
			
			return NORMAL;
		}
		if (PREFERENCIAL.sigla.equals(sigla)) {
			
			return Tipo.PREFERENCIAL;
		}
		
		throw new Invalid();
	}
	
	public int getPrioridade() {
		
		return prioridade;
	}

	public String getSigla() {
		
		return sigla;
	}

}
