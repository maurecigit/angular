package br.com.app.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Código não localizado.")
public class CodeNotFind extends RuntimeException {

	private static final long serialVersionUID = 1L;

}