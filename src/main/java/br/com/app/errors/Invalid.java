package br.com.app.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Tipo errado.")
public class Invalid extends RuntimeException {

	private static final long serialVersionUID = 1L;

}