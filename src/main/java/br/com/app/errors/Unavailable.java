package br.com.app.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Serviço indisponível.")
public class Unavailable extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
