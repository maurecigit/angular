package br.com.app.facade;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.app.domain.Password;
import br.com.app.domain.Tipo;
import br.com.app.errors.CodeNotFind;
import br.com.app.errors.PasswordNotFind;
import br.com.app.errors.PasswordsNoCall;
import br.com.app.service.PasswordService;

@RestController
@RequestMapping(value = "/senha", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PasswordController {

	@Autowired
	private SimpMessageSendingOperations message;

	@Autowired
	private PasswordService passwordService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		
		binder.registerCustomEditor(Tipo.class, new PropertyEditorSupport() {
			
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				
				setValue(Tipo.getBySigla(text));
			}
		});
		binder.registerCustomEditor(Password.class, new PropertyEditorSupport() {
			
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				
				setValue(new Password(text));
			}
		});
	}

	/**
	 * deleta a senha passada como parametro.
	 */
	@RequestMapping(value = "/{senha}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable Password password) {
		
		passwordService.delPassword(password);
	}
	
	/**
	 * Retorna a proxima senha, ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 * @return Senha da fila
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public Password nextPassword() {
		
		Password password = passwordService.nextPassword().orElseThrow(PasswordsNoCall::new);
		message.convertAndSend("/topic/senha", password);
		
		return password;		
	}
	
	/**
	 * Obter uma nova senha pois o Cliente clicou em gerar senha.
	 * 
	 * @param tipo
	 * @return Senha
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/{tipo}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<Password> newPassword(@PathVariable Tipo tipo, UriComponentsBuilder uriComponentsBuilder) {
		
		Password password = passwordService.newPassword(tipo);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/senha/{codigo}").buildAndExpand(password.getCodigo()).toUri());
		
		return new ResponseEntity<Password>(password, headers, HttpStatus.CREATED);
	}

	/**
	 * Gerente clicou em reiniciar senha.
	 * 
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/{tipo}")
	@ResponseStatus(value = HttpStatus.OK)
	public void restartPassword(@PathVariable Tipo tipo) {
		
		passwordService.resetPassword(tipo);
	}

	/**
	 * Localizar senha passada por parámentro
	 */
	@RequestMapping(value = "/{senha}", method = RequestMethod.GET)
	public Password findPassword(@PathVariable Password password) {
		
		return passwordService.findPassword(password).orElseThrow(CodeNotFind::new);
	}

	/**
	 * Retorna  ultima senha chamada pelo atendente.
	 * 
	 * @return List<Senha>
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Password lastCallPassword() {
		
		return passwordService.findLastCallPassword().orElseThrow(PasswordNotFind::new);
	}

}