package br.com.app.model;

import java.util.concurrent.PriorityBlockingQueue;

import org.springframework.stereotype.Repository;

import br.com.app.domain.Password;

@Repository
public class PasswordModel {

	private static PriorityBlockingQueue<Password> passwords = new PriorityBlockingQueue<>(10, (o1, o2) -> {
		
		return Integer.compare(o1.getPrioridade(), o2.getPrioridade());
	});

	/**
	 * Retorna a proxima senha, ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 * @return Senha da fila
	 */
	public Password firstPassword() {
		
		return passwords.poll();
	}

	/**
	 * deleta a senha passada como parametro.
	 */
	public boolean remove(Password password) {
		
		return passwords.remove(password);
	}
	
	public Password find(Password password) {
		
		if (passwords.contains(password)) {
			
			return password;
		}
		
		return null;
	}

	public void deleteAllInBatch() {
		
		passwords.clear();
	}
	
	/**
	 * Obter uma nova senha pois o Cliente clicou em gerar senha.
	 * 
	 * @param tipo
	 * @return Senha
	 */
	public void save(Password password) {
		
		passwords.add(password);
	}

}
