package br.com.app.service;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.app.domain.Password;
import br.com.app.domain.Tipo;
import br.com.app.model.PasswordModel;

@Service
public class PasswordService {


	private static AtomicInteger accountPriority = new AtomicInteger(0);
	private static AtomicInteger accountNormal = new AtomicInteger(0);
	private static Password lastCallPassword;

	@Autowired
	private PasswordModel model;
	
	/**
	 * Retorna a proxima senha, ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 * @return Senha da fila
	 */
	public Optional<Password> nextPassword() {
		
		Optional<Password> result = Optional.ofNullable(model.firstPassword());
		
		if (result.isPresent()) {
			
			lastCallPassword = result.get();
		}
		
		return result;
	}
	
	/**
	 * Localizar senha passada por parámentro
	 */
	public Optional<Password> findPassword(Password password) {
		
		return Optional.ofNullable(model.find(password));
	}
	
	/**
	 * Retorna  ultima senha chamada pelo atendente.
	 * 
	 * @return List<Senha>
	 */
	public Optional<Password> findLastCallPassword() {
		
		return Optional.ofNullable(lastCallPassword);
	}
	
	/**
	 * Reseta todas as senhas.
	 */
	public void deletePasswords() {
		
		resetPassword(Tipo.PREFERENCIAL);
		resetPassword(Tipo.NORMAL);
		model.deleteAllInBatch();
	}

	/**
	 * Retorna contador de senha, que sera necessaria para formar a senha.
	 * @param tp
	 * @return
	 */
	private AtomicInteger getAccount(Tipo tp) {
		
		if (Tipo.NORMAL.equals(tp)) {
			
			return accountNormal;			
		}
		
		return accountPriority;
	}

	/**
	 * deleta a senha passada como parametro.
	 */
	public void delPassword(Password password) {
		
		model.remove(password);
	}

	/**
	 * Gerente clicou em reiniciar senha.
	 * 
	 */
	public void resetPassword(Tipo tipo) {
		
		getAccount(tipo).set(0);
	}
	
	/**
	 * Obter uma nova senha pois o Cliente clicou em gerar senha.
	 * 
	 * @param tipo
	 * @return Senha
	 */
	public Password newPassword(Tipo tp) {
		
		if (tp == null) {
			
			throw new IllegalArgumentException();
		}
		
		Password password = new Password(tp, getAccount(tp).incrementAndGet());
		model.save(password);
		
		return password;
	}
}
