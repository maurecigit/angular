package br.com.app.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import br.com.app.Application;
import br.com.app.domain.Password;
import br.com.app.domain.Tipo;
import br.com.app.service.PasswordService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PasswordTest {
	
	private static Password PREFERENCIAL = new Password(Tipo.PREFERENCIAL, 1);
	private static Password NORMAL1 = new Password(Tipo.NORMAL, 1);
	private static Password NORMAL2 = new Password(Tipo.NORMAL, 2);
	
	private MockMvc mock;

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private PasswordService serv;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		
		this.mock = webAppContextSetup(webApplicationContext).build();
		this.serv.deletePasswords();
	}

	private void expect(ResultActions teste, Password password) throws Exception {
		
		teste.andExpect(content().contentType(contentType)).andExpect(jsonPath("$.tipo", is(password.getTipo().name())))
				.andExpect(jsonPath("$.sequencial", is(password.getSequencial())))
				.andExpect(jsonPath("$.codigo", is(password.getCodigo())))
				.andExpect(jsonPath("$.prioridade", is(password.getPrioridade())));
	}

	/**
	 * Testar reiniciar senha. Ou seja o gerente clicou em reiniciar senha.
	 * 
	 */
	@Test
	public void resetPassoword() throws Exception {
		
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL1);
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL2);
		mock.perform(put("/senha/N")).andExpect(status().isOk());
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL1);
	}
	
	
	/**
	 * Testar retornar a proxima senha com sucesso, pois ja foi chamada outra Ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 */
	@Test
	public void followPasswordCall() throws Exception {
		
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL1);
		expect(mock.perform(post("/senha")).andExpect(status().isOk()), NORMAL1);
		expect(mock.perform(get("/senha")).andExpect(status().isOk()), NORMAL1);
	}
	
	/**
	 * Testa retornar a proxima senha com erro. Ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 */
	@Test
	public void nextPasswordNoExist() throws Exception {
		
		mock.perform(post("/senha")).andExpect(status().isNotFound());
	}
	
	/**
	 * Testa retornar a proxima senha com sucesso. Ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 */
	@Test
	public void nextPasswordExist() throws Exception {
		
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL1);
		expect(mock.perform(post("/senha/P")).andExpect(status().isCreated()), PREFERENCIAL);
		expect(mock.perform(post("/senha")).andExpect(status().isOk()), PREFERENCIAL);
	}
	
	/**
	 * Testar retornar a proxima senha com sucesso. Ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 */
	@Test
	public void newPassword() throws Exception {
		
		expect(mock.perform(post("/senha/N")).andExpect(status().isCreated()), NORMAL1);
		expect(mock.perform(post("/senha/P")).andExpect(status().isCreated()), PREFERENCIAL);
	}

	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

	/**
	 * Testa retornar a proxima senha com erro, pois nenhuma foi chamada. Ou seja o Gerente clicou no botao `Proxima senha` para atender uma nova pessoa da fila.
	 * 
	 */
	@Test
	public void passwordNoCalled() throws Exception {
		
		mock.perform(get("/senha")).andExpect(status().isNotFound());
	}
}